#include "robot.h"

using namespace robot;

robot::SurfaceImpl::SurfaceImpl()
{
	this->card = nullptr;
	this->pos.x = 0;
	this->pos.y = 0;
	this->size_x = 0;
	this->size_y = 0;
	this->start.x = 0;
	this->start.y = 0;
	this->finish.x = 0;
	this->finish.y = 0;
}

uint SurfaceImpl::move(Point a)
{
	pos.x = a.x; pos.y = a.y;
	return distance(a);
}

std::vector<std::tuple<Point, uint>> SurfaceImpl::lookup()
	{
		std::vector<std::tuple<Point, uint>> look;
		std::vector <Point> points{ {pos.x + 1, pos.y}, {pos.x, pos.y - 1}, {pos.x, pos.y + 1}, {pos.x - 1, pos.y} };
		
		for(std::vector <Point>::iterator q = points.begin(); q != points.end(); q++)
			if (distance(*q) != -1)	look.push_back(std::make_tuple(*q, distance(*q)));

		return look;
	}

void SurfaceImpl::change(std::vector<Point> q)
{
	for (uint i = 0; i < q.size(); i++)
	{
		card[q[i].y][q[i].x] = '*';
	}
}

SurfaceImpl::~SurfaceImpl()
{
	free(this->card);
}

std::istream & robot::operator >> (std::istream & fin, SurfaceImpl & map)
{
	std::string line = "";
	uint i = 0, j = 0, length = 0;
	

	while (std::getline(fin, line)) { i++; };
	map.size_y = i;
	
	map.card = new char*[i]; 

	fin.clear();
	fin.seekg(0, std::ios::beg);

	i = 0;

	while (std::getline(fin, line)) 
	{
		length = line.size();
		map.card[i] = new char[length];
		for (j = 0; j <= length; j++)
		{
			map.card[i][j] = line[j];
			if (line[j] == 'S') { map.start.x = j; map.start.y = i; map.pos.x = j; map.pos.y = i; }
			if (line[j] == 'F') { map.finish.x = j; map.finish.y = i; }
		};
		i++;
	};

	map.size_x = length;
	return fin;
}

std::ostream & robot::operator<<(std::ostream & fout, const SurfaceImpl & map)
{
	for (uint i = 0; i < map.size_y; i++)
	{
		fout << map.card[i];
		fout << '\n';
	}
	return fout;
}

int Planar::distance(Point & a)
{
	int dis = 0;
	if ((a.x >= size_x) || (a.y >= size_y))	dis = -1;
	else if (card[a.y][a.x] == '#') dis = -1;
	else dis = abs((int)this->finish.x - (int)a.x) + abs((int)this->finish.y - (int)a.y);
	return dis;
}

int Cylinder::distance(Point & a)
{
	int dis = 0;
	a.x = (a.x + size_x) % size_x;
	if (a.y >= size_y) dis = -1;
	else if (card[a.y][a.x] == '#') dis = -1;
	else dis = abs((int)this->finish.y - (int)a.y) + std::min(std::min(abs((int)a.x - (int)this->finish.x), abs((int)a.x + ((int)this->size_x - (int)this->finish.x))), abs((int)this->finish.x + ((int)this->size_x - (int)a.x)));
	return dis;
}

int Tor::distance(Point & a)
{
	int dis = 0;
	a.x = (a.x + size_x) % size_x;
	a.y = (a.y + size_y) % size_y;
	if (card[a.y][a.x] == '#') dis = -1;
	else dis = std::min(std::min(abs((int)a.y - (int)this->finish.y), abs((int)a.y + ((int)this->size_y - (int)this->finish.y))), abs((int)this->finish.y + ((int)this->size_y - (int)a.y))) + std::min(std::min(abs((int)a.x - (int)this->finish.x), abs((int)a.x + ((int)this->size_x - (int)this->finish.x))), abs((int)this->finish.x + ((int)this->size_x - (int)a.x)));
	return dis;
}

Robot::Robot(Surface * a)
{
	he = a;
}

std::vector<Point> Robot::find(uint lim)
{
	std::vector<Point> result;
	std::vector<std::tuple<Point, uint>> vision;
	uint asd = 0, minim;
	Point now;

	while (asd<= lim)
	{
		vision = he->lookup();
		for (std::vector<std::tuple<Point, uint>>::iterator q = vision.begin(); q != vision.end(); q++)
			if (q == vision.begin()) { minim = std::get<1>(*q); now = std::get<0>(*q); }
			else if (std::get<1>(*q) < minim) { minim = std::get<1>(*q); now = std::get<0>(*q); };
		if (he->move(now) == 0) break;
		else { he->move(now); result.push_back(now); }
		asd++;
	}
	return result;
}

robot::Robot::~Robot()
{
	he = nullptr;
}
