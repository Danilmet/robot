#include "robot.h"

using namespace robot;

int main(){
	
	Point xy{ 4, 4 };

	const std::string s1 = "help", s2 = "space", s3 = "out", s4 = "limit", s5 = "topology";
	uint  limit = 1000;
	int i = 0, k = 0;
	std::string start = "", space = "space.txt", out = "route.txt", topology = "planar";

	std::getline(std::cin, start);
	start += '\n';
	while (start[i] != '\n') {
		if (start[i] == ' ') {
			k = i;
			while (start[k - 1] != '\n') {
				start[k] = start[k + 1];
				k++;
			}
			if (start[k] = '\n') start[k] = NULL;
		}
		else i++;
	}
	i = 0; 
	if (start[0] == '\n') std::cout << "You didn't give any keys(You can use -h or --help to see more info).\nSo We will use default settings.\n\n";
	else while (start[i] != '\n') {
			if ((start[i] == '-') & (start[i + 1] != '-')) {
				i++;
				if (start[i] == 'h') { std::cout << "\t\t\t\tHelp of the programm ""Robot""\n\t -s or --space    : Name of txt with description of space. Default: space.txt\n\t -o or --out      : The route of the final text file. Default: route.txt\n\t -l or --limit    : The number of maximum distance length. Default: 1000\n\t -t or --topology : Space topology. Default: planar"; return END; }
				if (start[i] == 's') { std::cout << "Name of txt with description of space: "; std::cin >> space; }
				if (start[i] == 'o') { std::cout << "The route of the final text file: "; std::cin >> out; }
				if (start[i] == 'l') { std::cout << "The number of maximum distance length - "; std::cin >> limit; }
				if (start[i] == 't') { std::cout << "Space topology: "; std::cin >> topology; }
				i++;
			}
			else if ((start[i] == '-') & (start[i + 1] == '-')) {
				i += 2;
				if (start.compare(i, 4, s1) == 0) { std::cout << "\t\t\t\tHelp of the programm ""Robot""\n\t -s or --space    : Name of txt with description of space. Default: space.txt\n\t -o or --out      : The route of the final text file. Default: route.txt\n\t -l or --limit    : The number of maximum distance length. Default: 1000\n\t -t or --topology : Space topology. Default: planar"; return END; }
				if (start.compare(i, 5, s2) == 0) { i += 5; std::cout << "Name of txt with description of space: "; std::cin >> space; }
				if (start.compare(i, 3, s3) == 0) { i += 3; std::cout << "The route of the final text file: "; std::cin >> out; }
				if (start.compare(i, 5, s4) == 0) { i += 5; std::cout << "The number of maximum distance length - "; std::cin >> limit; }
				if (start.compare(i, 8, s5) == 0) { i += 8; std::cout << "Space topology: "; std::cin >> topology; }
			}
			else i++;
		}

	GenericObjectFactory<std::string, SurfaceImpl> factory;
	factory.add<Planar>("planar");
	factory.add<Cylinder>("cylinder");
	factory.add<Tor>("tor");
	SurfaceImpl *map = factory.get(topology)();


	std::ifstream fin (space.c_str());
	if (fin.is_open()) {
		fin >> *map;
		Robot rb(map);
		map->change(rb.find(limit));
		std::ofstream fout(out.c_str());
		fout << *map;
	}
	else std::cout << "Can't open file!\n";
	
	return END;
}

