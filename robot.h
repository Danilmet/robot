#pragma once
#include <fstream>
#include <iostream>
#include <string>
#include <vector>
#include <tuple>
#include <cmath>
#include <algorithm>
#include <map>

#define uint unsigned int
#define END 0

namespace robot {
	class BadMove : public std::exception {};
	
	struct Point { uint x; uint y; };
	
	class Surface {
	public:
		
	//	���������� � ����� �������� ����� � ���������� ���������� �� ���� � ����� �����		
		virtual uint move(Point point) = 0;
		
	//	���������� �� ���� ��� ��������  �����  � ������� �������� �����������		
		virtual std::vector<std::tuple<Point, uint>> lookup() = 0;
	}; 

	class SurfaceImpl : public Surface 
	{
	public:
		SurfaceImpl();
		uint move(Point);
		std::vector<std::tuple<Point, uint>> lookup();
		friend std::istream& operator >> (std::istream&, SurfaceImpl&);
		friend std::ostream& operator << (std::ostream&, const SurfaceImpl&);
		void change(std::vector<Point>);
		virtual int distance(Point &) = 0;
		~SurfaceImpl();
	protected:
		Point finish, start, pos;
		size_t size_x, size_y;
		char ** card;
	};
	
	class Planar : public SurfaceImpl 
	{
	public:
		int distance(Point &);
	};
	
	class Cylinder : public SurfaceImpl 
	{
	public:	
		int distance(Point &);
	};
	
	class Tor : public SurfaceImpl 
	{
	public:	
		int distance(Point &);
	};

	class Robot 
	{
	public:
		Robot(Surface *);
		std::vector<Point> find (uint);
		~Robot();
	private:
		Surface * he;

	};
	
	template<class ID, class Base> class GenericObjectFactory {  //����� ������� �������� �������� 
	private:
		typedef Base* (*fInstantiator)();
		template<class Derived> static Base* instantiator() {
			return new Derived;
		}
		std::map<ID, fInstantiator> classes;

	public:
		GenericObjectFactory() {}
		template<class Derived> void add(ID id) {
			classes[id] = &instantiator<Derived>;
		}
		fInstantiator get(ID id) {
			return classes[id];
		}

	};


}
